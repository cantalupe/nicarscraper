
var GoogleSpreadsheet = require('google-spreadsheet');
var async = require('async');

// spreadsheet key is the long id in the sheets URL
var doc = new GoogleSpreadsheet('14zQ6cn4K_nImjchRc9sxAk46qwuMb_VA8puMCa4NSBc');
var sheet;
var dominios = [];
async.series([

    function setAuth(step) {
        //console.log('setAuth');
        // see notes below for authentication instructions!
        var creds = require('./NicScraper-577dab06ceaf.json');
        doc.useServiceAccountAuth(creds, step);
        //  console.log(step);
    },
    
    function getInfoAndWorksheets(step) {
        //console.log('getInfoAndWorksheets');
        doc.getInfo(function(err, info) {
            //console.log('Loaded doc: '+info.title+' by '+info.author.email);
            sheet = info.worksheets[0];
            //console.log('sheet 1: '+sheet.title+' '+sheet.rowCount+'x'+sheet.colCount);
            step();
        });
    },
    function workingWithRows(step) {
        sheet.getRows({
            offset: 1,
            limit: 20,
            orderby: 'col2'
        }, function( err, rows ){
            //console.log('Read '+rows.length+' rows');
            for (var i = 0, len = rows.length; i < len; i++) {
                scrape(rows[i].dominios).then((value) => {
                    console.log(value); // Success!
                });
                //dominios.push(rows[i].dominios);
            };
       
            step(err,rows);
            });
    },
], function(err,rows){
    if( err ) {
        console.log('Error: '+err);
    }
    return rows;
});

const puppeteer = require('puppeteer');

let scrape = async (dominio) => {

    const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage();
    await page.goto('https://nic.ar/acceso');
   
    // Scrape
    console.log('Arrancamos:');
    const newPagePromise = new Promise(x => browser.once('targetcreated', target => x(target.page())));   
    
    await page.waitForSelector("body > div > div.main-container.container.js-quickedit-main-content > div > section > div > article > div > div > div.ingreso-registro > div.tengo-clave > div.botoncito > div > a");
    console.log('Boton de ingreso con clave ...');
    await page.click('body > div > div.main-container.container.js-quickedit-main-content > div > section > div > article > div > div > div.ingreso-registro > div.tengo-clave > div.botoncito > div > a');

    const popup = await newPagePromise;
    console.log(popup.url());
    console.log('Cerramos pestaña vieja ...');
    page.close();

    console.log('Espero y completo campo de usuario ...');
    await popup.waitForSelector("#F1\\:username");
    await popup.evaluate(() => {
        document.querySelector("#F1\\:username").value = '20286929266';
    });

    await popup.waitForSelector('#F1\\:btnSiguiente');
    console.log('Click en siguiente ...');
    await popup.click('#F1\\:btnSiguiente');

    console.log('Espero y completo campo de password ...');
    await popup.waitForSelector("#F1\\:password");
    await popup.evaluate(() => {
      document.querySelector("#F1\\:password").value = 'Ijt6210492C';
    });
      //  await popup.waitFor(1000);
    await popup.waitForSelector('#F1\\:btnIngresar');
    console.log('Click en Ingresar ...');
    await popup.click('#F1\\:btnIngresar');
    //await popup.waitFor(15000);

    await popup.waitForSelector("body > app-root > app-header > div > div > div > div > div.col-sm-8 > div > div > ng-select > div > div > div.toggle"); // Dropdown Representante
    await popup.waitFor(500);
    console.log('Click en Dropdown de Representante ...');
    await popup.click('body > app-root > app-header > div > div > div > div > div.col-sm-8 > div > div > ng-select > div > div > div.toggle');

    
    await popup.waitForSelector('body > app-root > app-header > div > div > div > div > div.col-sm-8 > div > div > ng-select > select-dropdown > div > div.options > ul > li:nth-child(1)'); //Primer Representante
    await popup.waitFor(500);
    console.log('Click en Representante Agencia Cantalupe ...');
    await popup.click('body > app-root > app-header > div > div > div > div > div.col-sm-8 > div > div > ng-select > select-dropdown > div > div.options > ul > li:nth-child(1)');

    await popup.waitForSelector('#block-system-main > div.container > div > div > ul > div:nth-child(1) > div > div > div.los-botones > a:nth-child(2)'); //Iniciar Tramite
    await popup.waitFor(500);
    console.log('Click en inicio de Tramite sobre dominios ...');
    await popup.click('#block-system-main > div.container > div > div > ul > div:nth-child(1) > div > div > div.los-botones > a:nth-child(2)');

    console.log('Espero a que cargue tabla de dominios ...');
    await popup.waitForSelector("#tablaDominios");
    await popup.waitFor(5000);

    await popup.waitForSelector("#edit-keys");
    await popup.waitFor(500);
    
    await popup.type('#edit-keys', dominio);
    await popup.waitFor(500);
    console.log('Busco Dominio... ');
    await popup.click('#edit-submit');
    await popup.waitFor(2500);
    //await popup.press('Enter');
    
    await popup.waitForSelector("#tablaDominios > tbody > tr > td:nth-child(6) > div > button.btn.btn-success.btn-sm");
    console.log('Click en Renovar... ');
    let renovarBtn = await popup.$("#tablaDominios > tbody > tr > td:nth-child(6) > div > button.btn.btn-success.btn-sm");
    await popup.waitFor(500);
    //await popup.click("#tablaDominios > tbody > tr > td:nth-child(6) > div > button.btn.btn-success.btn-sm");
    await renovarBtn.click();
    await popup.waitFor(18000);

    await popup.waitForSelector("#sectionTramite > div.wizard > form > ul > a");
    await popup.waitFor(500);
    console.log('Click en Confirmar Tramite... ');
    await popup.click("#sectionTramite > div.wizard > form > ul > a");
    await popup.waitFor(2500);

    await popup.waitForSelector("body > app-root > op-payment-landing > div > div.row > div.col-md-8.order-md-1 > form > op-hub > div > op-payment-group-pick > div > div > div.d-flex.flex-row.justify-content-center.flex-wrap > a:nth-child(2)");
    await popup.waitFor(500);
    console.log('Pago con Tarjeta... ');
    await popup.click("body > app-root > op-payment-landing > div > div.row > div.col-md-8.order-md-1 > form > op-hub > div > op-payment-group-pick > div > div > div.d-flex.flex-row.justify-content-center.flex-wrap > a:nth-child(2)");
    await popup.waitFor(2500);
    
    await popup.waitForSelector("body > app-root > op-payment-landing > div > div.row > div.col-md-8.order-md-1 > form > op-hub > div > div > op-decidirv1 > div > op-card-picker > div > div:nth-child(2) > div.d-flex.flex-column.w-100 > div > a:nth-child(2)");
    await popup.waitFor(500);
    console.log('Visa... ');
    await popup.click("body > app-root > op-payment-landing > div > div.row > div.col-md-8.order-md-1 > form > op-hub > div > div > op-decidirv1 > div > op-card-picker > div > div:nth-child(2) > div.d-flex.flex-column.w-100 > div > a:nth-child(2)");
    await popup.waitFor(2500);

    await popup.waitForSelector("#plans");
    await popup.waitFor(500);
    console.log('1 Cuota... ');
    await popup.select('#plans', '0: 1')
    //await popup.click("#plans > option");
    await popup.waitFor(2500);

    await popup.waitForSelector("body > app-root > op-payment-landing > div > div.row > div.col-md-8.order-md-1 > form > op-hub > div > div > op-decidirv1 > div > div > button");
    await popup.waitFor(500);
    console.log('Continuar... ');
    await popup.click("body > app-root > op-payment-landing > div > div.row > div.col-md-8.order-md-1 > form > op-hub > div > div > op-decidirv1 > div > div > button");
    await popup.waitFor(2500);
    
    console.log('Ingreso TITULAR TARJETA ...');
    await popup.waitForSelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(1) > td:nth-child(2) > font.texto > input");
    await popup.evaluate(() => {
        document.querySelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(1) > td:nth-child(2) > font.texto > input").value = 'Juana Serra';
    });

    console.log('Ingreso NUMERO TARJETA ...');
    await popup.waitForSelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(2) > td:nth-child(2) > font > input");
    await popup.waitFor(500);
    await popup.evaluate(() => {
        document.querySelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(2) > td:nth-child(2) > font > input").value = '4937150000674831';
    });

    console.log('Ingreso FECHA VENCIMIENTO ...');
    await popup.waitForSelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(3) > td:nth-child(2) > font > input");
    await popup.waitFor(500);
    await popup.evaluate(() => {
        document.querySelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(3) > td:nth-child(2) > font > input").value = '0819';
    });

    console.log('Ingreso CODDIGO SEGURIDAD ...');
    await popup.waitForSelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(4) > td:nth-child(2) > font > input[type=password]");
    await popup.waitFor(500);
    await popup.evaluate(() => {
        document.querySelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(4) > td:nth-child(2) > font > input[type=password]").value = '596';
    });

    console.log('Ingreso EMAIL ...');
    await popup.waitForSelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > font > input");
    await popup.waitFor(500);
    await popup.evaluate(() => {
        document.querySelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > font > input").value = 'administracion@agenciacantalupe.com';
    });

    /*await popup.waitForSelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(6) > td:nth-child(2) > select");
    await popup.waitFor(500);
    console.log('Ingreso TIPODOC... ');
    await popup.select('#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(6) > td:nth-child(2) > select', '1')
    //await popup.click("#plans > option");
    await popup.waitFor(20000);*/

    console.log('Ingreso EMAIL ...');
    await popup.waitForSelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(7) > td:nth-child(2) > font > input");
    await popup.waitFor(500);
    await popup.evaluate(() => {
        document.querySelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(7) > td:nth-child(2) > font > input").value = '36528031';
    });

    console.log('Ingreso EMAIL ...');
    await popup.waitForSelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(8) > td:nth-child(2) > font > input");
    await popup.waitFor(500);
    await popup.evaluate(() => {
        document.querySelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(8) > td:nth-child(2) > font > input").value = 'Cabildo';
    });

    console.log('Ingreso EMAIL ...');
    await popup.waitForSelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > font > input");
    await popup.waitFor(500);
    await popup.evaluate(() => {
        document.querySelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > font > input").value = '1543';
    });
    console.log('Ingreso EMAIL ...');
    await popup.waitForSelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(10) > td:nth-child(2) > font > input");
    await popup.waitFor(500);
    await popup.evaluate(() => {
        document.querySelector("#DatosTarjetaForm > table:nth-child(1) > tbody > tr:nth-child(10) > td:nth-child(2) > font > input").value = '11111991';
    });

    await popup.waitForSelector("#DatosTarjetaForm > table:nth-child(2) > tbody > tr > td > input[type=SUBMIT]:nth-child(3)");
    await popup.waitFor(500);
    console.log('ACEPTAR... ');
    await popup.click("#DatosTarjetaForm > table:nth-child(2) > tbody > tr > td > input[type=SUBMIT]:nth-child(3)");
    await popup.waitFor(2500);

    return result;
    browser.close();
};



